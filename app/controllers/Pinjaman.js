var express         = require('express'),
    app             = express(),
    router          = express.Router(),
    mongoose        = require('mongoose'),
    randomstring    = require("randomstring"),
    tokenLogin      = mongoose.model('tokenLoginAdmin'),
    tokenLoginAtas  = mongoose.model('tokenLoginAtasan'),
    kendaraan       = mongoose.model('Kendaraan'),
    pinjaman        = mongoose.model('Pinjaman'),
    driver          = mongoose.model('Driver'),
    histori         = mongoose.model('Histori'),
    atasan          = mongoose.model('Atasan'),
    admin           = mongoose.model('Admin');
var moment          = require('moment');
var jwt             = require('jsonwebtoken');
const { json } = require('body-parser');

module.exports = function (app) {
    app.use('/v1/rent/', router);
};

router.post('/',function(req,res,next){
    let heder       =   req.header('Authorization');
    var data        =   tokenLogin.findOne({Token:heder});
    data.exec(function(err,resultToken){
        if(resultToken  !=  null){
            jwt.verify(heder,'Login',function(err,data){
                if(err){
                    res.json({
                        hasil   :   false,
                        message :   "login dulus"
                    })
                } else {
                    var data2   =   admin.findOne({_id:data.username});
                    data2.exec(function(err,resultAdmin){
                        if(resultAdmin  !=  null){
                            var data3   =   kendaraan.findOne({platnomor:req.body.platnomor});
                            data3.exec(function(err,resultKendaraan){
                                if(resultKendaraan  !=  null){
                                    var data4   =   driver.findOne({Username:req.body.driver});
                                    data4.exec(function(err,resultDriver){
                                        if(resultDriver !=  null){
                                            var today   =   moment(moment(new Date()), 'MM/DD/YYYY').format();
                                            var awal    =   req.body.Ambil;
                                            var akhir   =   req.body.Kembali;
                                            var data5   =   pinjaman.findOne({$and:[{Mobil:resultKendaraan._id},{Driver:resultDriver._id},{$or:[  {$and:[{Ambil:{$lte:awal}},{Kambalikan:{$gte:awal}}]} ,   {$and:[{Ambil:{$gte:awal}},{Kambalikan:{$lte:awal}}]}   ,   {$and:[{Ambil:{$lte:akhir}},{Kambalikan:{$gte:akhir}}]} ,   {$and:[{Ambil:{$gte:akhir}},{Kambalikan:{$lte:akhir}}]} ,   {$and:[{Ambil:{$gte:awal}},{Kambalikan:{$lte:akhir}}]} ]}]});
                                            data5.exec(function(err,resultPinjaman){
                                                if(resultPinjaman   ==  null){
                                                    var data6   =   atasan.findOne({Username:req.body.Approve1});
                                                    data6.exec(function(err,resultApprove1){
                                                        if(resultApprove1   !=  null){
                                                            if(resultApprove1.Jabatan   ==  1){
                                                                var data7   =   atasan.findOne({Username:req.body.Approve2});
                                                                data7.exec(function(err,resultApprove2){
                                                                    if(resultApprove2   !=  null){
                                                                        if(resultApprove2.Jabatan   ==  2){
                                                                            cariKode(function(randoms){
                                                                                var input   =   new pinjaman({
                                                                                                    Mobil       :   resultKendaraan._id,
                                                                                                    Driver      :   resultDriver._id,
                                                                                                    Kode        :   randoms,
                                                                                                    Approve1    :   resultApprove1._id,
                                                                                                    Approve2    :   resultApprove2._id,
                                                                                                    Status      :   "Pending",
                                                                                                    Ambil       :   awal,
                                                                                                    Kambalikan  :   akhir,
                                                                                                    CreatedBy   :   data.username,
                                                                                                    CreatedAt   :   today
                                                                                                })
                                                                                input.save(function(err,result2){
                                                                                    var input2  =   new histori({
                                                                                                        Pinjaman    :   result2._id,
                                                                                                        Driver      :   resultDriver._id,
                                                                                                        Mobil       :   resultKendaraan._id,
                                                                                                        Status      :   "Pending",
                                                                                                        CreatedAt   :   today,
                                                                                                        CreatedBy   :   data.username
                                                                                                    })
                                                                                    input2.save(function(err,resultz){})
                                                                                    res.json({
                                                                                        hasil   :   true,
                                                                                        message :   "Berhasil"
                                                                                    })
                                                                                })
                                                                            })
                                                                        } else {
                                                                            res.json({
                                                                                hasil   :   false,
                                                                                message :   "Approve 2 tidak punya akses"
                                                                            })
                                                                        }
                                                                    } else {
                                                                        res.json({
                                                                            hasil   :   false,
                                                                            message :   "Approve 2 tidak ada"
                                                                        })
                                                                    }
                                                                })
                                                            } else {
                                                                res.json({
                                                                    hasil   :   false,
                                                                    message :   "Approve 1 tidak punya akses"
                                                                })
                                                            }
                                                        } else {
                                                            res.json({
                                                                hasil   :   false,
                                                                message :   "Approve 1 tidak ada"
                                                            })
                                                        }
                                                    })
                                                } else {
                                                    if(resultPinjaman.Mobil ==  resultKendaraan._id){
                                                        res.json({
                                                            hasil   :   false,
                                                            message :   "Mobil dipinjam"
                                                        })
                                                    } else {
                                                        res.json({
                                                            hasil   :   false,
                                                            message :   "Driver ada jadwal"
                                                        })
                                                    }
                                                }
                                            })
                                        } else {
                                            res.json({
                                                hasil   :   false,
                                                message :   "Driver tidak ada"
                                            })
                                        }
                                    })
                                } else {
                                    res.json({
                                        hasil   :   false,
                                        message :   "Kendaraan sudah ada"
                                    })
                                }
                            })
                        } else {
                            res.json({
                                hasil   :   false,
                                message :   "Login dulu"
                            })
                        }
                    })
                }
            })
        } else {
            res.json({
                hasil   :   false,
                message :   "Login dulu token"
            })
        }
    })
})

function cariKode(callback){
    let random = randomstring.generate({
        length : 11,
        charset : 'alphabetic',
        capitalization :'uppercase'
    })
    let stop = true;
    while(stop == false){
        var data2 = pinjaman.findOne({KodeUdangan:random});
        data2.exec(function(err,result){
            if(result == null){
                stop = true
            }
        })
    }
    callback(random);
}

router.post('/update',function(req,res,next){
    let heder   =   req.header('Authorization');
    var data    =   tokenLoginAtas.findOne({Token:heder});
    var today   =   moment(moment(new Date()), 'MM/DD/YYYY').format();
    data.exec(function(err,resultToken){
        if(resultToken  !=  null){
            jwt.verify(heder,'Login',function(err,data){
                if(err){
                    res.json({
                        hasil   :   false,
                        message :   "login dulus"
                    })
                } else {
                    var data2   =   atasan.findOne({_id:data.username});
                    data2.exec(function(err,resultAtasan){
                        if(resultAtasan  !=  null){
                            if(resultAtasan.Jabatan ==  1){
                                var data3   =   pinjaman.findOne({$and:[{Approve1:resultAtasan._id},{Kode:req.body.Kode}]});
                                data3.exec(function(err,resultPinjaman){
                                    if(resultPinjaman   !=  null){
                                        if(resultPinjaman.Status    ==  "Pending"){
                                            var input   =   new histori({
                                                                Pinjaman    :   resultPinjaman._id,
                                                                Driver      :   resultPinjaman.Driver,
                                                                Mobil       :   resultPinjaman.Mobil,
                                                                Status      :   "Approve1",
                                                                CreatedAt   :   today,
                                                                CreatedByS  :   data.username
                                                            })
                                            input.save(function(err,resultz){})
                                            var dataold =   {   _id     :   resultPinjaman._id};
                                            var datanew =   {
                                                                Status      :   "Approve1",
                                                                EditedAt    :   today
                                                            };
                                            var options = {};
                                            pinjaman.update(dataold , datanew , options , callback);
                                            function callback(err , result){}
                                            res.json({
                                                hasil   :   true,
                                                message :   "Berhasil"
                                            })
                                        } else {
                                            res.json({
                                                hasil   :   false,
                                                message :   "Gagal Update status"
                                            })
                                        }
                                    }
                                })
                            } else {
                                var data3   =   pinjaman.findOne({$and:[{Approve2:resultAtasan._id},{Kode:req.body.Kode}]});
                                data3.exec(function(err,resultPinjaman){
                                    if(resultPinjaman   !=  null){
                                        if(resultPinjaman.Status    ==  "Approve1"){
                                            var input   =   new histori({
                                                                Pinjaman    :   resultPinjaman._id,
                                                                Driver      :   resultPinjaman.Driver,
                                                                Mobil       :   resultPinjaman.Mobil,
                                                                Status      :   "Approve2",
                                                                CreatedAt   :   today,
                                                                CreatedByS  :   data.username
                                                            })
                                            input.save(function(err,resultz){})
                                            var dataold =   {   _id     :   resultPinjaman._id};
                                            var datanew =   {
                                                                Status      :   "Approve2",
                                                                EditedAt    :   today
                                                            };
                                            var options = {};
                                            pinjaman.update(dataold , datanew , options , callback);
                                            function callback(err , result){}
                                            res.json({
                                                hasil   :   true,
                                                message :   "Berhasil"
                                            })
                                        } else {
                                            res.json({
                                                hasil   :   false,
                                                message :   "Gagal Update status"
                                            })
                                        }
                                    }
                                })
                            }
                        } else {
                            res.json({
                                hasil   :   false,
                                message :   "Login dulu"
                            })
                        }
                    })
                }
            })
        } else {
            res.json({
                hasil   :   false,
                message :   "Login dulu token"
            })
        }
    })
})

router.post('/diagram',function(req,res,next){
    let heder       =   req.header('Authorization');
    var data        =   tokenLogin.findOne({Token:heder});
    data.exec(function(err,resultToken){
        if(resultToken  !=  null){
            jwt.verify(heder,'Login',function(err,data){
                if(err){
                    res.json({
                        hasil   :   false,
                        message :   "login dulus"
                    })
                } else {
                    var data2   =   admin.findOne({_id:data.username});
                    data2.exec(function(err,resultAdmin){
                        if(resultAdmin  !=  null){
                            var today   =   moment(moment(new Date()), 'MM/DD/YYYY').format();
                            var data3   =   histori.find({$and:[{CreatedAt:{$lte:today}},{Status:"Approve2"}]}).sort({Mobil:-1}).populate({
                                path    :   'Mobil',
                                model   :   'Kendaraan'
                            });
                            data3.exec(function(err,result){
                                let hasils  =   [];
                                let Mobil   =   [];
                                let Jumlah  =   [];
                                let loop    =   1;
                                if(result.length    !=  0){
                                    let pinjams;
                                    for(var i = 0; i < result.length; i++){
                                        if(result.length    ==  1){
                                            pinjams =   {
                                                "Mobil" :   result[i].Mobil.platnomor,
                                                "Jumlah":   loop
                                            }
                                            hasils.push(pinjams);
                                            Mobil.push(pinjams.Mobil)
                                            Jumlah.push(pinjams.Jumlah)
                                        } else {
                                            if(i    ==  0){
                                                pinjams =   {
                                                    "Mobil" :   result[i].Mobil.platnomor,
                                                    "Jumlah":   loop
                                                }
                                            } else{
                                                if (result[i - 1].Mobil.platnomor   ==  result[i].Mobil.platnomor){
                                                    loop            =   loop    +   1;
                                                    pinjams =   {
                                                        "Mobil" :   result[i].Mobil.platnomor,
                                                        "Jumlah":   loop
                                                    }
                                                } else {
                                                    Mobil.push(pinjams.Mobil)
                                                    Jumlah.push(pinjams.Jumlah)
                                                    hasils.push(pinjams);
                                                    loop = 1;
                                                }
                                                if(result.length    -   1   ==  i){
                                                    Mobil.push(pinjams.Mobil)
                                                    Jumlah.push(pinjams.Jumlah)
                                                    hasils.push(pinjams);
                                                }
                                            }
                                        }
                                    }
                                    res.json({
                                        hasil   :   true,
                                        result  :   hasils,
                                        mobils  :   Mobil,
                                        jumls  :   Jumlah
                                    })
                                }
                            })
                        } else {
                            res.json({
                                hasil   :   false,
                                message :   "Login dulu"
                            })
                        }
                    })
                }
            })
        } else {
            res.json({
                hasil   :   false,
                message :   "Login dulu token"
            })
        }
    })
})

router.post('/histori',function(req,res,next){
    let heder       =   req.header('Authorization');
    var data        =   tokenLogin.findOne({Token:heder});
    data.exec(function(err,resultToken){
        if(resultToken  !=  null){
            jwt.verify(heder,'Login',function(err,data){
                if(err){
                    res.json({
                        hasil   :   false,
                        message :   "login dulus"
                    })
                } else {
                    var data2   =   admin.findOne({_id:data.username});
                    data2.exec(function(err,resultAdmin){
                        if(resultAdmin  !=  null){
                            var data3   =   pinjaman.find({}).sort({CreatedAt:-1}).populate({
                                path    :   'Mobil',
                                model   :   'Kendaraan'
                            }).populate({
                                path    :   'Driver',
                                model   :   'Driver'
                            }).populate({
                                path    :   'Approve1',
                                model   :   'Atasan'
                            }).populate({
                                path    :   'Approve2',
                                model   :   'Atasan'
                            });
                            data3.exec(function(err,resultPinjaman){
                                let hasils   =   [];
                                for(var i = 0; i < resultPinjaman.length; i++){
                                    if(i < resultPinjaman.length - 2){
                                        let asd =   {
                                            "Mobil"         :   resultPinjaman[i].Mobil.platnomor,
                                            "Driver"        :   resultPinjaman[i].Driver.FullName,
                                            "Ambil"         :   resultPinjaman[i].Ambil,
                                            "Kembalikan"    :   resultPinjaman[i].Kambalikan,
                                            "Status"        :   resultPinjaman[i].Status
                                        }
                                        hasils.push(asd)
                                    } else {
                                        let TotalMobil  = [...new Set(resultPinjaman.map(item => item.Mobil))];
                                        let TotalDriver = [...new Set(resultPinjaman.map(item => item.Driver))];
                                        let asd =   {
                                            "TotalMobil"    :   TotalMobil.length,
                                            "TotalDriver"   :   TotalDriver.length
                                        }
                                        hasils.push(asd)
                                    }
                                }
                                res.json({
                                    hasil   :   true,
                                    result  :   hasils
                                })
                            })
                        } else {
                            res.json({
                                hasil   :   false,
                                message :   "Login dulu"
                            })
                        }
                    })
                }
            })
        } else {
            res.json({
                hasil   :   false,
                message :   "Login dulu token"
            })
        }
    })
})

router.post('/listbyatasan',function(req,res,next){
    let heder       =   req.header('Authorization');
    var data        =   tokenLoginAtas.findOne({Token:heder});
    var today       =   moment(moment(new Date()), 'MM/DD/YYYY').format();
    data.exec(function(err,resultToken){
        console.log(resultToken)
        if(resultToken  !=  null){
            jwt.verify(heder,'Login',function(err,data){
                if(err){
                    res.json({
                        hasil   :   false,
                        message :   "login dulus"
                    })
                } else {
                    var data2   =   atasan.findOne({_id:data.username});
                    data2.exec(function(err,resultAdmin){
                        if(resultAdmin  !=  null){
                            if(resultAdmin.Jabatan  ==  1){
                                var data3   =   pinjaman.find({$and:[{Approve1:resultAdmin._id},{Ambil:{$gte:today}},{Status:"Pending"}]}).sort({Ambil:-1}).populate({
                                    path    :   'Mobil',
                                    model   :   'Kendaraan'
                                }).populate({
                                    path    :   'Driver',
                                    model   :   'Driver'
                                }).populate({
                                    path    :   'Approve1',
                                    model   :   'Atasan'
                                }).populate({
                                    path    :   'Approve2',
                                    model   :   'Atasan'
                                });
                                data3.exec(function(err,resultPinjaman){
                                    let hasils   =   [];
                                    for(var i = 0; i < resultPinjaman.length; i++){
                                        let asd =   {
                                            "Mobil"         :   resultPinjaman[i].Mobil.platnomor,
                                            "Driver"        :   resultPinjaman[i].Driver.FullName,
                                            "Ambil"         :   resultPinjaman[i].Ambil,
                                            "Kembalikan"    :   resultPinjaman[i].Kambalikan,
                                            "Status"        :   resultPinjaman[i].Status,
                                            "Kode"          :   resultPinjaman[i].Kode
                                        }
                                        hasils.push(asd)
                                    }
                                    res.json({
                                        hasil   :   true,
                                        results :   resultPinjaman,
                                        result  :   hasils
                                    })
                                })
                            } else {
                                var data3   =   pinjaman.find({$and:[{Approve2:resultAdmin._id},{Ambil:{$gte:today}},{Status:"Approve1"}]}).sort({Ambil:-1}).populate({
                                    path    :   'Mobil',
                                    model   :   'Kendaraan'
                                }).populate({
                                    path    :   'Driver',
                                    model   :   'Driver'
                                }).populate({
                                    path    :   'Approve1',
                                    model   :   'Atasan'
                                }).populate({
                                    path    :   'Approve2',
                                    model   :   'Atasan'
                                });
                                data3.exec(function(err,resultPinjaman){
                                    let hasils   =   [];
                                    for(var i = 0; i < resultPinjaman.length; i++){
                                        let asd =   {
                                            "Mobil"         :   resultPinjaman[i].Mobil.platnomor,
                                            "Driver"        :   resultPinjaman[i].Driver.FullName,
                                            "Ambil"         :   resultPinjaman[i].Ambil,
                                            "Kembalikan"    :   resultPinjaman[i].Kambalikan,
                                            "Status"        :   resultPinjaman[i].Status,
                                            "Kode"          :   resultPinjaman[i].Kode
                                        }
                                        hasils.push(asd)
                                    }
                                    res.json({
                                        hasil   :   true,
                                        results :   resultPinjaman,
                                        result  :   hasils
                                    })
                                })
                            }
                        } else {
                            res.json({
                                hasil   :   false,
                                message :   "Login dulu"
                            })
                        }
                    })
                }
            })
        } else {
            res.json({
                hasil   :   false,
                message :   "Login dulu token"
            })
        }
    })
})

router.post('/historibyatasan',function(req,res,next){
    let heder       =   req.header('Authorization');
    var data        =   tokenLoginAtas.findOne({Token:heder});
    data.exec(function(err,resultToken){
        if(resultToken  !=  null){
            jwt.verify(heder,'Login',function(err,data){
                if(err){
                    res.json({
                        hasil   :   false,
                        message :   "login dulus"
                    })
                } else {
                    var data2   =   atasan.findOne({_id:data.username});
                    data2.exec(function(err,resultAdmin){
                        if(resultAdmin  !=  null){
                            var data3   =   histori.find({CreatedByS:resultAdmin._id}).sort({CreatedAt:-1}).populate({
                                path    :   'Mobil',
                                model   :   'Kendaraan'
                            }).populate({
                                path    :   'Driver',
                                model   :   'Driver'
                            }).populate({
                                path    :   'Pinjaman',
                                model   :   'Pinjaman'
                            });
                            data3.exec(function(err,resultPinjaman){
                                let hasils   =   [];
                                for(var i = 0; i < resultPinjaman.length; i++){
                                    let asd =   {
                                        "Mobil"         :   resultPinjaman[i].Mobil.platnomor,
                                        "Driver"        :   resultPinjaman[i].Driver.FullName,
                                        "Ambil"         :   resultPinjaman[i].Pinjaman.Ambil,
                                        "Kembalikan"    :   resultPinjaman[i].Pinjaman.Kambalikan,
                                        "Status"        :   resultPinjaman[i].Pinjaman.Status
                                    }
                                    hasils.push(asd)
                                }
                                res.json({
                                    hasil   :   true,
                                    result  :   hasils
                                })
                            })
                        } else {
                            res.json({
                                hasil   :   false,
                                message :   "Login dulu"
                            })
                        }
                    })
                }
            })
        } else {
            res.json({
                hasil   :   false,
                message :   "Login dulu token"
            })
        }
    })
})