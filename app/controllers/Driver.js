var express         = require('express'),
    app             = express(),
    router          = express.Router(),
    mongoose        = require('mongoose'),
    tokenLogin      = mongoose.model('tokenLoginDriver'),
    driver          = mongoose.model('Driver');
var moment          = require('moment');
const bcrypt        = require('bcrypt');
const saltRounds    = 10;
var jwt             = require('jsonwebtoken');

module.exports = function (app) {
    app.use('/v1/driver/', router);
};

router.post('/register',function(req,res,next){
    var data    =   driver.findOne({Username:req.body.Username});
    data.exec(function(err,result){
        if(result   ==  null){
            var pswd = req.body.Password;
            bcrypt.hash(pswd, saltRounds, function(err, hash) {
                var input   =   new driver({
                                    Username    :   req.body.Username,
                                    Password    :   hash,
                                    FullName    :   req.body.FullName,
                                    Active      :   1,
                                    CreatedAt   :   moment(moment(new Date()), 'MM/DD/YYYY').format()
                                })
                input.save(function(err,result2){
                    res.json({
                        hasil   :   true,
                        message :   "Berhasil"
                    })
                })
            });
        } else {
            res.json({
                hasil   :   false,
                message :   "Username atau Nomor HP sudah digunakan"
            })
        }
    })
})

router.post('/login',function(req,res,next){
    var data    =   driver.findOne({Username    :   req.body.Username});
    data.exec(function(err,result){
        if(result   !=  null){
            bcrypt.compare(req.body.Password, result.Password,function(err,compare){
                if(compare  ==  true){
                    var today   =   moment(moment(new Date()), 'MM/DD/YYYY').format();
                    var token   =   jwt.sign({
                                        username    :   result._id,
                                        today       :   today
                                    }, 'Login');
                    var data2   =   tokenLogin.findOne({Username:result._id});
                    data2.exec(function(err,result2){
                        if(result2  ==  null){
                            var input   =   new tokenLogin({
                                                Token       :   token,
                                                Driver      :   result._id,
                                                CreatedAt   :   today
                                            })
                            input.save(function(err,result){
                                res.json({
                                    hasil   :   true,
                                    message :   "Berhasil",
                                    token   :   token
                                })
                            })
                        } else {
                            var dataold =   {   _id :   result2._id};
                            var datanew =   {
                                                token       :   token,
                                                CreatedAt   :   today
                                            };
                            var options = {};
                            tokenLogin.update(dataold , datanew , options , callback);
                            function callback(err , result){
                                res.json({
                                    hasil   :   true,
                                    message :   "Berhasil",
                                    token   :   token
                                })
                            }
                        }
                    })
                } else {
                    res.json({
                        hasil   :   false,
                        message :   "Password salah",
                        token   :   ""
                    })
                }
            })
        } else {
            res.json({
                hasil   :   false,
                message :   "Username Tidak Ada",
                token   :   ""
            })
        }
    })
})

router.post('/cekparams',function(req,res,next){
    let heder       =   req.header('Authorization');
    var data        =   tokenLogin.findOne({token:heder});
    data.exec(function(err,resultToken){
        if(resultToken  !=  null){
            jwt.verify(heder,'Login',function(err,data){
                if(err){
                    res.json({
                        hasil   :   false,
                        message :   "login dulu"
                    })
                } else {
                    var data2   =   atasan.findOne({_id:data.Atasan});
                    data2.exec(function(err,resultAtasan){
                        if(resultAtasan  !=  null){
                            res.json({
                                hasil   :   true,
                                message :   "Berhasil",
                                result  :   resultAtasan
                            })
                        } else {
                            res.json({
                                hasil   :   false,
                                message :   "Login dulu"
                            })
                        }
                    })
                }
            })
        } else {
            res.json({
                hasil   :   false,
                message :   "Login dulu"
            })
        }
    })
})

router.post('/listactive',function(req,res,next){
    var data    =   driver.find({Active:1});
    data.exec(function(err,resultDriver){
        res.json({
            hasil   :   true,
            result  :   resultDriver
        })
    })
})