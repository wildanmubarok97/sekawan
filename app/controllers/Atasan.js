var express         = require('express'),
    app             = express(),
    router          = express.Router(),
    mongoose        = require('mongoose'),
    tokenLogin      = mongoose.model('tokenLoginAtasan'),
    atasan          = mongoose.model('Atasan');
var moment          = require('moment');
const bcrypt        = require('bcrypt');
const saltRounds    = 10;
var jwt             = require('jsonwebtoken');
var cors            = require('cors');

module.exports = function (app) {
    app.use(cors());
    app.use('/v1/atasan/', router);
};

router.post('/register',function(req,res,next){
    var data    =   atasan.findOne({Username:req.body.Username});
    data.exec(function(err,result){
        if(result   ==  null){
            var pswd = req.body.Password;
            bcrypt.hash(pswd, saltRounds, function(err, hash) {
                var input   =   new atasan({
                                    Username    :   req.body.Username,
                                    Password    :   hash,
                                    FullName    :   req.body.FullName,
                                    Jabatan     :   req.body.Jabatan,
                                    Active      :   1,
                                    CreatedAt   :   moment(moment(new Date()), 'MM/DD/YYYY').format()
                                })
                input.save(function(err,result2){
                    res.json({
                        hasil   :   true,
                        message :   "Berhasil"
                    })
                })
            });
        } else {
            res.json({
                hasil   :   false,
                message :   "Username atau Nomor HP sudah digunakan"
            })
        }
    })
})

router.post('/login',function(req,res,next){
    var data    =   atasan.findOne({Username    :   req.body.Username});
    data.exec(function(err,result){
        if(result   !=  null){
            bcrypt.compare(req.body.Password, result.Password,function(err,compare){
                if(compare  ==  true){
                    var today   =   moment(moment(new Date()), 'MM/DD/YYYY').format();
                    var token   =   jwt.sign({
                                        username    :   result._id,
                                        today       :   today
                                    }, 'Login');
                    var data2   =   tokenLogin.findOne({Atasan:result._id});
                    data2.exec(function(err,result2){
                        if(result2  ==  null){
                            var input   =   new tokenLogin({
                                                Token       :   token,
                                                Atasan      :   result._id,
                                                CreatedAt   :   today
                                            })
                            input.save(function(err,result){
                                res.json({
                                    hasil   :   true,
                                    message :   "Berhasil",
                                    token   :   token
                                })
                            })
                        } else {
                            var dataold =   {   _id :   result2._id};
                            var datanew =   {
                                                Token       :   token,
                                                CreatedAt   :   today
                                            };
                            var options = {};
                            tokenLogin.update(dataold , datanew , options , callback);
                            function callback(err , result){
                                res.json({
                                    hasil   :   true,
                                    message :   "Berhasil",
                                    token   :   token
                                })
                            }
                        }
                    })
                } else {
                    res.json({
                        hasil   :   false,
                        message :   "Password salah",
                        token   :   ""
                    })
                }
            })
        } else {
            res.json({
                hasil   :   false,
                message :   "Username Tidak Ada",
                token   :   ""
            })
        }
    })
})

router.post('/cekparams',function(req,res,next){
    let heder       =   req.header('Authorization');
    console.log(heder)
    var data        =   tokenLogin.findOne({Token:heder});
    data.exec(function(err,resultToken){
        if(resultToken  !=  null){
            jwt.verify(heder,'Login',function(err,data){
                if(err){
                    res.json({
                        hasil   :   false,
                        message :   "login dulu"
                    })
                } else {
                    var data2   =   atasan.findOne({_id:data.username});
                    data2.exec(function(err,resultAtasan){
                        console.log(data)
                        if(resultAtasan  !=  null){
                            res.json({
                                hasil   :   true,
                                message :   "Berhasil",
                                result  :   resultAtasan
                            })
                        } else {
                            res.json({
                                hasil   :   false,
                                message :   "Login dulu"
                            })
                        }
                    })
                }
            })
        } else {
            res.json({
                hasil   :   false,
                message :   "Login dulu"
            })
        }
    })
})

router.post('/list',function(req,res,next){
    var data    =   atasan.find({Jabatan:req.body.Jabatan});
    data.exec(function(err,result){
        res.json({
            hasil   :   true,
            result  :   result
        })
    })
})