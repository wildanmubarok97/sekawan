var express         = require('express'),
    app             = express(),
    router          = express.Router(),
    mongoose        = require('mongoose'),
    tokenLogin      = mongoose.model('tokenLoginAdmin'),
    admin           = mongoose.model('Admin');
var moment          = require('moment');
const bcrypt        = require('bcrypt');
const saltRounds    = 10;
var jwt             = require('jsonwebtoken');

module.exports = function (app) {
    app.use('/v1/admin/', router);
};

router.post('/register',function(req,res,next){
    var data    =   admin.findOne({Username:req.body.Username});
    data.exec(function(err,result){
        if(result   ==  null){
            var pswd = req.body.Password;
            bcrypt.hash(pswd, saltRounds, function(err, hash) {
                var input   =   new admin({
                                    Username    :   req.body.Username,
                                    Password    :   hash,
                                    FullName    :   req.body.FullName,
                                    Active      :   1,
                                    CreatedAt   :   moment(moment(new Date()), 'MM/DD/YYYY').format()
                                })
                input.save(function(err,result2){
                    res.json({
                        hasil   :   true,
                        message :   "Berhasil"
                    })
                })
            });
        } else {
            res.json({
                hasil   :   false,
                message :   "Username atau Nomor HP sudah digunakan"
            })
        }
    })
})

router.post('/login',function(req,res,next){
    var data    =   admin.findOne({Username    :   req.body.Username});
    data.exec(function(err,result){
        if(result   !=  null){
            bcrypt.compare(req.body.Password, result.Password,function(err,compare){
                if(compare  ==  true){
                    var today   =   moment(moment(new Date()), 'MM/DD/YYYY').format();
                    var token   =   jwt.sign({
                                        username    :   result._id,
                                        today       :   today
                                    }, 'Login');
                    var data2   =   tokenLogin.findOne({$and:[{Username:result._id},{Type:'admin'}]});
                    data2.exec(function(err,result2){
                        if(result2  ==  null){
                            var input   =   new tokenLogin({
                                                Token       :   token,
                                                Admin       :   result._id,
                                                CreatedAt   :   today
                                            })
                            input.save(function(err,result){
                                res.json({
                                    hasil   :   true,
                                    message :   "Berhasil",
                                    token   :   token
                                })
                            })
                        } else {
                            var dataold =   {   _id :   result2._id};
                            var datanew =   {
                                                Token       :   token,
                                                CreatedAt   :   today
                                            };
                            var options = {};
                            tokenLogin.update(dataold , datanew , options , callback);
                            function callback(err , result){
                                res.json({
                                    hasil   :   true,
                                    message :   "Berhasil",
                                    token   :   token
                                })
                            }
                        }
                    })
                } else {
                    res.json({
                        hasil   :   false,
                        message :   "Password salah",
                        token   :   ""
                    })
                }
            })
        } else {
            res.json({
                hasil   :   false,
                message :   "Username Tidak Ada",
                token   :   ""
            })
        }
    })
})

router.post('/cekparams',function(req,res,next){
    let heder       =   req.header('Authorization');
    var data        =   tokenLogin.findOne({Token:heder});
    data.exec(function(err,resultToken){
        if(resultToken  !=  null){
            jwt.verify(heder,'Login',function(err,data){
                if(err){
                    res.json({
                        hasil   :   false,
                        message :   "login dulu"
                    })
                } else {
                    var data2   =   admin.findOne({_id:data.username});
                    data2.exec(function(err,resultAdmin){
                        if(resultAdmin  !=  null){
                            res.json({
                                hasil   :   true,
                                message :   "Berhasil",
                                result  :   resultAdmin
                            })
                        } else {
                            res.json({
                                hasil   :   false,
                                message :   "Login dulu"
                            })
                        }
                    })
                }
            })
        } else {
            res.json({
                hasil   :   false,
                message :   "Login dulu"
            })
        }
    })
})