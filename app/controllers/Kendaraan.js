var express         = require('express'),
    app             = express(),
    router          = express.Router(),
    mongoose        = require('mongoose'),
    tokenLogin      = mongoose.model('tokenLoginAdmin'),
    kendaraan       = mongoose.model('Kendaraan'),
    admin           = mongoose.model('Admin');
var moment          = require('moment');
var jwt             = require('jsonwebtoken');

module.exports = function (app) {
    app.use('/v1/kendaraan/', router);
};

router.post('/add',function(req,res,next){
    let heder       =   req.header('Authorization');
    var data        =   tokenLogin.findOne({Token:heder});
    data.exec(function(err,resultToken){
        if(resultToken  !=  null){
            jwt.verify(heder,'Login',function(err,data){
                if(err){
                    res.json({
                        hasil   :   false,
                        message :   "login dulus"
                    })
                } else {
                    var data2   =   admin.findOne({_id:data.username});
                    data2.exec(function(err,resultAdmin){
                        if(resultAdmin  !=  null){
                            var data3   =   kendaraan.findOne({platnomor:req.body.platnomor});
                            data3.exec(function(err,resultKendaraan){
                                if(resultKendaraan  ==  null){
                                    var input   =   new kendaraan({
                                                        platnomor   :   req.body.platnomor,
                                                        merek       :   req.body.merek,
                                                        type        :   req.body.type,
                                                        tahun       :   req.body.tahun,
                                                        Active      :   1,
                                                        CreatedBy   :   data.username,
                                                        CreatedAt   :   moment(moment(new Date()), 'MM/DD/YYYY').format()
                                                    })
                                    input.save(function(err,result2){
                                        res.json({
                                            hasil   :   true,
                                            message :   "Berhasil"
                                        })
                                    })
                                } else {
                                    res.json({
                                        hasil   :   false,
                                        message :   "Kendaraan sudah ada"
                                    })
                                }
                            })
                        } else {
                            res.json({
                                hasil   :   false,
                                message :   "Login dulu"
                            })
                        }
                    })
                }
            })
        } else {
            res.json({
                hasil   :   false,
                message :   "Login dulu token"
            })
        }
    })
})

router.post('/list',function(req,res,next){
    var data    =   kendaraan.find({});
    data.exec(function(err,result){
        res.json({
            hasil   :   true,
            result  :   result
        })
    })
})