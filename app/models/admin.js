var mongoose = require('mongoose'),
Schema = mongoose.Schema;
var AdminSchema = new Schema({
    Username    : {
        type    : String
    },
    Password    : {
        type    : String
    },
    FullName    : {
        type    : String
    },
    Active      : {
        type    : Number
    },
    CreatedAt   : {
        type    : String
    },
    updatedAt   : {
        type    : String
    }

})
mongoose.model('Admin', AdminSchema);