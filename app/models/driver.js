var mongoose = require('mongoose'),
Schema = mongoose.Schema;
var DriverSchema = new Schema({
    Username    : {
        type    : String
    },
    Password    : {
        type    : String
    },
    FullName    : {
        type    : String
    },
    Active      : {
        type    : Number
    },
    CreatedAt   : {
        type    : String
    },
    updatedAt   : {
        type    : String
    }   

})
mongoose.model('Driver', DriverSchema);