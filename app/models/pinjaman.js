var mongoose = require('mongoose'),
Schema = mongoose.Schema;
var PinjamanSchema = new Schema({
    Mobil       : {
        type    : Schema.Types.ObjectId,
        ref     : 'Kendaraan'
    },
    Driver      : {
        type    : Schema.Types.ObjectId,
        ref     : 'Driver'
    },
    Approve1    : {
        type    : Schema.Types.ObjectId,
        ref     : 'Atasan'
    },
    Approve2    : {
        type    : Schema.Types.ObjectId,
        ref     : 'Atasan'
    },
    Status      : {
        type    : String
    },
    Kode        : {
        type    : String
    },
    Ambil       : {
        type    : String
    },
    Kambalikan  : {
        type    : String
    },
    CreatedAt   : {
        type    : String
    },
    CreatedBy   : {
        type    : Schema.Types.ObjectId,
        ref     : 'Admin'
    },
    updatedAt   : {
        type    : String
    }   

})
mongoose.model('Pinjaman', PinjamanSchema);