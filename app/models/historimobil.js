var mongoose = require('mongoose'),
Schema = mongoose.Schema;
var HistoriSchema = new Schema({
    Pinjaman    : {
        type    : Schema.Types.ObjectId,
        ref     : 'Pinjaman'
    },
    Driver      : {
        type    : Schema.Types.ObjectId,
        ref     : 'Driver'
    },
    Mobil      : {
        type    : Schema.Types.ObjectId,
        ref     : 'Kendaraan'
    },
    Status      : {
        type    : String
    },
    CreatedAt   : {
        type    : String
    },
    CreatedBy   : {
        type    : Schema.Types.ObjectId,
        ref     : 'Admin'
    },
    CreatedByS  : {
        type    : Schema.Types.ObjectId,
        ref     : 'Atasan'
    },
    updatedAt   : {
        type    : String
    }   

})
mongoose.model('Histori', HistoriSchema);