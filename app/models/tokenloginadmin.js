var mongoose = require('mongoose'),
Schema = mongoose.Schema;
var tokenLoginAdminSchema = new Schema({
    Token           : {
        type        : String
    },
    Admin           : {
        type        : Schema.Types.ObjectId,
        ref         : 'Admin'
    },
    CreatedAt       : {
        type        : String
    },
    EditedAt        : {
        type        : String
    }
})
mongoose.model('tokenLoginAdmin', tokenLoginAdminSchema);