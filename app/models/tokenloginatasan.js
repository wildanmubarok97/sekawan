var mongoose = require('mongoose'),
Schema = mongoose.Schema;
var tokenLoginAtasanSchema = new Schema({
    Token       : {
        type    : String
    },
    Atasan      : {
        type    : Schema.Types.ObjectId,
        ref     : 'Atasan'
    },
    CreatedAt   : {
        type    : String
    },
    EditedAt    : {
        type    : String
    }
})
mongoose.model('tokenLoginAtasan', tokenLoginAtasanSchema);