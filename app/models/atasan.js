var mongoose = require('mongoose'),
Schema = mongoose.Schema;
var AtasanSchema = new Schema({
    Username    : {
        type    : String
    },
    Password    : {
        type    : String
    },
    FullName    : {
        type    : String
    },
    Jabatan     : {
        type    : Number
    },
    Active      : {
        type    : Number
    },
    CreatedAt   : {
        type    : String
    },
    updatedAt   : {
        type    : String
    }   

})
mongoose.model('Atasan', AtasanSchema);