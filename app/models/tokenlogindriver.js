var mongoose = require('mongoose'),
Schema = mongoose.Schema;
var tokenLoginDriveranSchema = new Schema({
    Token       : {
        type    : String
    },
    Driver      : {
        type    : Schema.Types.ObjectId,
        ref     : 'Driver'
    },
    CreatedAt   : {
        type    : String
    },
    EditedAt    : {
        type    : String
    }
})
mongoose.model('tokenLoginDriver', tokenLoginDriveranSchema);