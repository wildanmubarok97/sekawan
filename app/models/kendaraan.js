var mongoose = require('mongoose'),
Schema = mongoose.Schema;
var KendaraanSchema = new Schema({
    platnomor   : {
        type    : String
    },
    merek       : {
        type    : String
    },
    type        : {
        type    : String
    },
    tahun       : {
        type    : String
    },
    Active      : {
        type    : Boolean
    },
    CreatedAt   : {
        type    : String
    },
    CreatedBy   : {
        type    : Schema.Types.ObjectId,
        ref     : 'Admin'
    },
    EditedAt    : {
        type    : String
    },
    EditedBy    : {
        type    : Schema.Types.ObjectId,
        ref     : 'Admin'
    }
})
mongoose.model('Kendaraan', KendaraanSchema);