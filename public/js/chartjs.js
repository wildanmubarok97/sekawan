let user = localStorage.getItem("tokensekawan");
$.ajax({
    type    :   "POST",
    url     :   "/sekawan/v1/rent/diagram",
    headers :   {
        "Authorization" :   user
    },
    success:function(datas){
        if(datas.hasil == true){
            var ctx = document.getElementById('myChart');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: datas.mobils,
                    datasets: [{
                        label: '# of Votes',
                        data: datas.jumls,
                        backgroundColor: [
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255, 206, 86, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            })
        }
    }
})