var users = localStorage.getItem("tokensekawan");

let tetep   =   false;
$('#submit').on('click',function(e){
    var dataSubmited  = $('#rent-form').serializeObject();
    e.preventDefault();
    $.ajax({
      type    :   "POST",
      url     :   "/sekawan/v1/rent",
      data    :   {
        platnomor     : dataSubmited.mobil,
        driver      : dataSubmited.driver,
        Approve1   : dataSubmited.approve1,
        Approve2   : dataSubmited.approve2,
        Ambil   : dataSubmited.ambil,
        Kembali     :   dataSubmited.kembalikan
      },
      headers: {
        "Authorization": users
      },    
      success:function(data){
        if(data.hasil   !=  true){
            alert(data.message)
        } else {
            window.location.href ='/sekawan/report';
        }
      }
    })
  })

$.ajax({
    type    :   "POST",
    url     :   "/sekawan/v1/atasan/list",
    data    :   {
        Jabatan        :   1
    },
    success:function(data){
        if(data.hasil == true){
            var data2 = data.result;
            $.each(data2 , function(i, item){
                var show =  '<option value="'   +   data2[i].Username   +   '">'    +  data2[i].FullName  +   '</option>' 
                $('#approve1').append(show)
            })
        } else {
            alert(data.message)
        }
    }
})

$.ajax({
    type    :   "POST",
    url     :   "/sekawan/v1/atasan/list",
    data    :   {
        Jabatan        :   2
    },
    success:function(data){
        if(data.hasil == true){
            var data2 = data.result;
            $.each(data2 , function(i, item){
                var show =  '<option value="'   +   data2[i].Username   +   '">'    +  data2[i].FullName  +   '</option>' 
                $('#approve2').append(show)
            })
        } else {
            alert(data.message)
        }
    }
})

$.ajax({
    type    :   "POST",
    url     :   "/sekawan/v1/driver/listactive",
    success:function(data){
        if(data.hasil == true){
            var data2 = data.result;
            $.each(data2 , function(i, item){
                var show =  '<option value="'   +   data2[i].Username   +   '">'    +  data2[i].FullName  +   '</option>' 
                $('#driver').append(show)
            })
        } else {
            alert(data.message)
        }
    }
})

$.ajax({
    type    :   "POST",
    url     :   "/sekawan/v1/kendaraan/list",
    success:function(data){
        if(data.hasil == true){
            var data2 = data.result;
            $.each(data2 , function(i, item){
                var show =  '<option value="'   +   data2[i].platnomor   +   '">'    +  data2[i].platnomor  +   '</option>' 
                $('#mobil').append(show)
            })
        } else {
            alert(data.message)
        }
    }
})