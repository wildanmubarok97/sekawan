var users = localStorage.getItem("tokensekawan");
if(users    !=  null    ||  users   !=  undefined){
    window.location.href ='/sekawan';
}
$('#submit-login').on('click' , function(e){
    var dataSubmited    =   $('#login-form').serializeObject();
    e.preventDefault();
    $.ajax({
        type    :   "POST",
        url     :   "/sekawan/v1/admin/login",
        data    :   {
            Username        :   dataSubmited.username,
            Password        :   dataSubmited.password
        },
        success:function(data){
            if(data.hasil == true){
                localStorage.setItem('tokensekawan', data.token);
                window.location.href ='/sekawan';
            } else {
                alert(data.message)
            }
        }
    })
})