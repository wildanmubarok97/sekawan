let user = localStorage.getItem("tokensekawan");
$.ajax({
    type    :   "POST",
    url     :   "/sekawan/v1/rent/histori",
    headers :   {
        "Authorization" :   user
    },
    success:function(datas){
        if(datas.hasil == true){
            let data = datas.result;
            $.each(data, function (i, item) {
                if(i < data.length - 2){
                    var show =
                        "<tr>"                  +
                            "<td>"              +
                                (i + 1)         +
                            "</td>"             +
                            "<td>"              +
                                data[i].Mobil    +
                            "</td>"             +
                            "<td>"              +
                                data[i].Driver    +
                            "</td>"             +
                            "<td>"              +
                                data[i].Ambil   +
                            "</td>"             +
                            "<td>"              +
                                data[i].Kembalikan  +
                            "</td>"             +
                            "<td>"              +
                                data[i].Status    +
                            "</td>"             +
                        "</tr>";
                    $("#home").append(show);
                } else if(i == data.length - 1) {
                    var show = "<tr style='display: none;'>"   +
                                    "<td><p style='display: none;'>"              +
                                        (i + 1)         +
                                    "</p></td>"             +
                                    "<td>Jumlah Mobil</td>" +
                                    "<td>"  +     data[i].TotalMobil    +   "</td>" +
                                    "<td></td>"         +
                                    "<td></td>"         +
                                    "<td></td>"         +
                                "</tr>"
                    $("#home").append(show);         
                } else {
                    var show = "<tr style='display: none;'>"   +
                                    "<td><p style='display: none;'>"              +
                                        (i + 1)         +
                                    "</p></td>"             +
                                    "<td>Jumlah Driver</td>" +
                                    "<td>"  +     data[i].TotalDriver    +   "</td>" +
                                    "<td></td>"         +
                                    "<td></td>"         +
                                    "<td></td>"         +
                                "</tr>"
                    $("#home").append(show);   
                }
            });
            var table = $("#lisTable").DataTable({
                lengthChange: false,
                buttons: [
                {
                    extend: "excel",
                    exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5],
                    },
                },
                {
                    extend: "pdf",
                    exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5],
                    },
                },
                {
                    extend: "print",
                    exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5],
                    },
                },
                "colvis",
                ],
            });

            table.buttons().container().appendTo("#lisTable_wrapper .col-md-6:eq(0)");
        }
    }
})