const path = require('path');
const rootPath = path.normalize(__dirname + '/..');
const env = process.env.NODE_ENV || 'development';

const config = {
  development: {
    root: rootPath,
    app: {
      name: 'sekawan'
    },
    port: process.env.PORT || 5000,
    db: 'mongodb://localhost/sekawan-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'sekawan'
    },
    port: process.env.PORT || 5000,
    db: 'mongodb://localhost/sekawan-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'sekawan'
    },
    port: process.env.PORT || 5000,
    db: 'mongodb://localhost/sekawan-production'
  }
};

module.exports = config[env];
